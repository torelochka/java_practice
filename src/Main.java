import itis.zheleznov.jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentsRepositoryJdbcImpl;

import java.sql.Connection;
import java.util.List;

public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USERNAME = "postgres";
    private static final String PASS = "qwerty007";

    public static void main(String[] args) {
        SimpleDataSource simpleDataSource = new SimpleDataSource();

        Connection connection = simpleDataSource.openConnection(URL, USERNAME, PASS);

        StudentsRepositoryJdbcImpl studentsRepositoryJdbc = new StudentsRepositoryJdbcImpl(connection);

        System.out.println("findAll");
        List<Student> students = studentsRepositoryJdbc.findAll();
        for (Student st : students) {
            System.out.println(st);
        }
        System.out.println("-------------");

        System.out.println("findById");
        Student student = studentsRepositoryJdbc.findById(2L);
        System.out.println(student);
        System.out.println("-------------");

        System.out.println("update");
        student.setAge(20);
        studentsRepositoryJdbc.update(student);
        System.out.println(studentsRepositoryJdbc.findById(2L));
        System.out.println("-------------");

        System.out.println("save");
        student = new Student(null, "�������", "������", 17, 902);
        System.out.println(student);
        studentsRepositoryJdbc.save(student);
        System.out.println(student);
        System.out.println("-------------");

        System.out.println("findAllByAge");
        students = studentsRepositoryJdbc.findAllByAge(19);
        for (Student st : students) {
            System.out.println(st);
        }
        System.out.println("-------------");

        simpleDataSource.closeConnection(connection);
    }
}
