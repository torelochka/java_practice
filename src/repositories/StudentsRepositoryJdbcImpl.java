package repositories;

import models.Mentor;
import models.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String ORDER_BY_S_ID_M_ID = " order by s.id, m.id";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select s.id as id, s.first_name as s_n, s.last_name as s_l_n," +
            " s.age as s_a, s.group_number as s_g, m.id, m.first_name as m_n, m.last_name as m_l_n from student s " +
            "left join mentor m on s.id = m.student_id";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " where s.id = ";

    //language=SQL
    private static final String SQL_SELECT_ALL_ORDER = SQL_SELECT_ALL + ORDER_BY_S_ID_M_ID;

    //language=SQL
    private static final String SQL_SELECT_BY_AGE = SQL_SELECT_ALL + " where s.age = ";

    //language=SQL
    private static final String SQL_INSERT = "insert into student (first_name, last_name, age, group_number) VALUES " +
            "(?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update Student set id = ?, first_name = ?, last_name = ?, " +
            "age = ?, group_number = ? where id = ?";

    private Connection connection;

    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Student> findAllByAge(int age) {
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT_BY_AGE + age + ORDER_BY_S_ID_M_ID);

            return selectHandler(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // ���������� �������� ������ ���� ���������, ��� ���� � ������� �������� ������ ���� ���������� ������ ��������
    // � �������� � ���� ������� ������ ����������� (����� �����, �������, id �� ����)
    // student1(id, firstName, ..., mentors = [{id, firstName, lastName, null}, {}, ), student2, student3
    // ��� ������� ����� ��������
    @Override
    public List<Student> findAll() {
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT_ALL_ORDER);

            return selectHandler(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public Student findById(Long id) {
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id + ORDER_BY_S_ID_M_ID);

            ArrayList<Student> students = selectHandler(resultSet);

            if (!students.isEmpty())
                return students.get(0);
            else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }


    // ������ ���������� insert ��� ��������
    // student = Student(null, '�������', '�������', 26, 915)
    // studentsRepository.save(student);
    // student = Student(3, '�������', '�������', 26, 915)
    @Override
    public void save(Student entity) {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(SQL_INSERT, 1);

            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setInt(3, entity.getAge());
            preparedStatement.setInt(4, entity.getGroupNumber());

            preparedStatement.executeUpdate();
            ResultSet id = preparedStatement.getGeneratedKeys();

            id.next();

            entity.setId(id.getLong(1));
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // ��� ��������, � ������� ����� id ��������� ���������� ���� �����

    // student = Student(3, '�������', '�������', 26, 915)
    // student.setFirstName("�����")
    // student.setLastName(null);
    // studentsRepository.update(student);
    // (3, '�����', null, 26, 915)
    @Override
    public void update(Student entity) {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE);

            //����� �� ������ ID ??

            preparedStatement.setLong(1, entity.getId());
            preparedStatement.setString(2, entity.getFirstName());
            preparedStatement.setString(3, entity.getLastName());
            preparedStatement.setInt(4, entity.getAge());
            preparedStatement.setInt(5, entity.getGroupNumber());
            preparedStatement.setLong(6, entity.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    private ArrayList<Student> selectHandler(ResultSet resultSet) {
        ArrayList<Mentor> mentors = new ArrayList<>();
        ArrayList<Student> students = new ArrayList<>();
        Student student = null;
        long tempId = 0;

        try {
            while (resultSet.next()) {
                if (tempId != resultSet.getLong(1)) {
                    tempId = resultSet.getLong(1);
                    mentors = new ArrayList<>();
                    student = new Student(
                            resultSet.getLong(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getInt(4),
                            resultSet.getInt(5),
                            mentors);
                    students.add(student);
                    if (resultSet.getLong(6) != 0) {
                        mentors.add(new Mentor(
                                resultSet.getLong(6),
                                resultSet.getString(7),
                                resultSet.getString(8),
                                student
                        ));
                    }
                } else {
                    mentors.add(new Mentor(
                            resultSet.getLong(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            student
                    ));
                }
            }
            return students;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
