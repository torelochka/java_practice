package models;

public class Mentor {
    private long id;
    private String first_name;
    private String last_name;
    private Student student;

    public Mentor(long id, String first_name, String last_name, Student student) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.student = student;
    }

    @Override
    public String toString() {
        return "Mentor{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                '}';
    }
}
