const myselfRoutes = require('./myself_routes');
const requestRoutes = require('./request_routes');
const requestsRoutes = require('./requests_routes');
module.exports = function (app) {
    myselfRoutes(app);
    requestRoutes(app);
    requestsRoutes(app);
};

