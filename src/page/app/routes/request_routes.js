bodyParser = require('body-parser').json();

module.exports = function (app) {

    app.post('/request', bodyParser, (request, response) => {
        const {Client} = require('pg')

        const client = new Client({
            user: 'postgres',
            host: 'localhost',
            database: 'node_js_requests',
            password: 'qwerty007',
        })

        client.connect()

        let name = `${request.body.name}`;
        let email = `${request.body.email}`;
        let text = `${request.body.text}`;
        let backButton = '<br><a href = "/"><button>Return</button></a>';

        let query = 'INSERT INTO node_js_requests.schema_name.requests (name, email, text) VALUES ($1, $2, $3);'
        client.query(query, [name, email, text], (err) => {
            if (err != null)
                response.send("Your request was rejected, please check the correct values." + backButton);
            else
                response.send("Your request is accepted, thank you !" + backButton);
            client.end();
        });
    });
};
