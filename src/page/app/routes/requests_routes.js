module.exports = function (app) {
    app.get('/requests', (request, response) => {
        const {Client} = require('pg')

        const client = new Client({
            user: 'postgres',
            host: 'localhost',
            database: 'node_js_requests',
            password: 'qwerty007',
        })

        client.connect()

        client.query('SELECT * FROM node_js_requests.schema_name.requests', (err, res) => {
            response.send(JSON.stringify(res.rows));
        });
    });
};
