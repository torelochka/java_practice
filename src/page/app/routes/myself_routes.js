module.exports = function (app) {
    app.get('/about_myself', (request, response) => {
        var result = [
            {
                "Name": "Dmitriy",
                "Last name": "Zheleznov",
                "Status": "Student",
                "Address": "35 Kremlevskaya str., Kazan, Russia",
                "Telephone": "+7 (123) 456-78-90",
                "Vk": "thunder_voice",
                "Info": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi laoreet vitae orci id <br>" +
                    "pharetra.\nNulla viverra dolor metus, vitae varius augue volutpat condimentum. Orci varius<br>" +
                    "natoque penatibus\net magnis dis parturient montes, nascetur ridiculus mus. In quam<br>" +
                    "enim, facilisis et maximus in,\n placerat nec sem."
            }
        ];
        response.send(JSON.stringify(result));
    });
};
