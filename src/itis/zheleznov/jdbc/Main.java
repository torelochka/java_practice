package itis.zheleznov.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USERNAME = "postgres";
    private static final String PASS = "qwerty007";

    public static void main(String[] args) throws SQLException {
        SimpleDataSource dataSource = new SimpleDataSource();

        Connection connection = dataSource.openConnection(URL, USERNAME, PASS);

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("Select * from good");

        while (resultSet.next()) {
            System.out.println("ID: " + resultSet.getInt("id"));
            System.out.println("Name: " + resultSet.getString("name"));
            System.out.println("Price:  " + resultSet.getInt("price"));
        }
        System.out.println("------------------");

        resultSet.close();

        resultSet = statement.executeQuery("select g.name as g_name, c.name as c_name from good as g " +
                "inner join category_has_good as ch on g.id = ch.good_id " +
                "inner join category as c on c.id = ch.category_id;");

        while (resultSet.next()) {
            System.out.println(resultSet.getString("g_name") + " " +
                    resultSet.getString("c_name"));
        }

        connection.close();

    }
}
